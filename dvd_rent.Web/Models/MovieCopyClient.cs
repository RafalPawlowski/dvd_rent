﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dvd_rent.Web.Models
{
    public class MovieCopyClient
    {
        public int ID { get; set; }
        public int ClientId { get; set; }
        public int MovieCopyId { get; set; }
        public DateTime? TakeDate { get; set; }
        public DateTime? BackDate { get; set; }
    }
}
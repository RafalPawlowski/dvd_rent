﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dvd_rent.Web.Models
{
    public class Client
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Street { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
    }
}